<?php

use yii\db\Migration;

/**
 * Handles the creation for table `paseo`.
 * Has foreign keys to the tables:
 *
 * - `perro`
 * - `arbol`
 */
class m160612_092508_create_paseo extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('paseo', [
            'id' => $this->primaryKey(),
            'perro_id' => $this->integer()->notNull(),
            'arbol_id' => $this->integer()->defaultValue(null),
            'etapa' => $this->string(10)->notNull(),
            'fecha' => $this->date()->notNull(),
        ]);

        // creates index for column `perro_id`
        $this->createIndex(
            'idx-paseo-perro_id',
            'paseo',
            'perro_id'
        );

        // add foreign key for table `perro`
        $this->addForeignKey(
            'fk-paseo-perro_id',
            'paseo',
            'perro_id',
            'perro',
            'id',
            'CASCADE'
        );

        // creates index for column `arbol_id`
        $this->createIndex(
            'idx-paseo-arbol_id',
            'paseo',
            'arbol_id'
        );

        // add foreign key for table `arbol`
        $this->addForeignKey(
            'fk-paseo-arbol_id',
            'paseo',
            'arbol_id',
            'arbol',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        // drops foreign key for table `perro`
        $this->dropForeignKey(
            'fk-paseo-perro_id',
            'paseo'
        );

        // drops index for column `perro_id`
        $this->dropIndex(
            'idx-paseo-perro_id',
            'paseo'
        );

        // drops foreign key for table `arbol`
        $this->dropForeignKey(
            'fk-paseo-arbol_id',
            'paseo'
        );

        // drops index for column `arbol_id`
        $this->dropIndex(
            'idx-paseo-arbol_id',
            'paseo'
        );

        $this->dropTable('paseo');
    }
}
