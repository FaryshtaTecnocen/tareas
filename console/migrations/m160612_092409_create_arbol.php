<?php

use yii\db\Migration;

/**
 * Handles the creation for table `arbol`.
 */
class m160612_092409_create_arbol extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('arbol', [
            'id' => $this->primaryKey(),
            'fecha_marcado' => $this->date()->defaultValue(null),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('arbol');
    }
}
