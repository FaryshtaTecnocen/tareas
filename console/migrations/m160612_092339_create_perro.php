<?php

use yii\db\Migration;

/**
 * Handles the creation for table `perro`.
 */
class m160612_092339_create_perro extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('perro', [
            'id' => $this->primaryKey(),
            'nombre' => $this->string(20)->notNull(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('perro');
    }
}
