<?php

namespace common\models;

/**
 * This is the model class for table "arbol".
 *
 * @property integer $id
 * @property integer $marcado
 *
 * @property Paseo[] $paseos
 */
class Arbol extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'arbol';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fecha_marcado'], 'date'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'fecha_marcado' => 'Fecha',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaseos()
    {
        return $this->hasMany(Paseo::className(), ['arbol_id' => 'id']);
    }
}
