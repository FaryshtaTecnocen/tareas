<?php

namespace common\models;

use common\workflows\PaseoWorkflow;
use common\workflows\behaviors\PaseoBehavior;
use raoul2000\workflow\base\SimpleWorkflowBehavior;
use raoul2000\workflow\validation\WorkflowValidator;
use raoul2000\workflow\validation\WorkflowScenario;

/**
 * This is the model class for table "paseo".
 *
 * @property integer $id
 * @property integer $perro_id
 * @property integer $arbol_id
 * @property string $fecha
 *
 * @property Arbol $arbol
 * @property Perro $perro
 */
class Paseo extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'paseo';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['perro_id', 'fecha'], 'required'],
            [
                ['arbol_id'],
                'required',
                'on' => WorkflowScenario::enterStatus(
                    PaseoWorkflow::WORKFLOW_OLFATEAR
                ),
            ],
            [['perro_id', 'arbol_id'], 'integer'],
            [['fecha'], 'date'],
            [['etapa'], WorkflowValidator::className()],
            [
                ['arbol_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Arbol::className(),
                'targetAttribute' => ['arbol_id' => 'id'],
            ],
            [
                ['perro_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Perro::className(),
                'targetAttribute' => ['perro_id' => 'id'],
            ],
        ];
    }

    public function behaviors()
    {
        return [
            'workflow' => [
                'class' => SimpleWorkflowBehavior::className(),
                'defaultWorkflowId'      => 'PaseoWorkFlow',
                'statusAttribute'        => 'etapa',
                'propagateErrorsToModel' => true,
                'propagateErrorsToModel' => true
            ],
            'paseoBehavior' => [
                'class' => PaseoBehavior::className(),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'perro_id' => 'Perro ID',
            'arbol_id' => 'Arbol ID',
            'fecha' => 'Fecha',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getArbol()
    {
        return $this->hasOne(Arbol::className(), ['id' => 'arbol_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPerro()
    {
        return $this->hasOne(Perro::className(), ['id' => 'perro_id']);
    }
}
