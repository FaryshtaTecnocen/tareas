<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "perro".
 *
 * @property integer $id
 * @property string $nombre
 *
 * @property Paseo[] $paseos
 */
class Perro extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'perro';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nombre'], 'required'],
            [['nombre'], 'string', 'max' => 20],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaseos()
    {
        return $this->hasMany(Paseo::className(), ['perro_id' => 'id']);
    }
}
