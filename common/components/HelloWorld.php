<?php

namespace common\components;

use yii\base\Event;

/**
 * Component que imprime un 'Hello World'
 */
class HelloWorld extends \yii\base\Component
{
    /**
     * Evento antes de imprimir.
     */
    const EVENT_BEFORE_PRINT = 'beforePrint';
    
    /**
     * @var string nombre del componente.
     */
    public $name = 'Angel';
    
    /**
     * Dispara el evento antes de imprimir.
     *
     * @return boolean si se debe continuar la impresión.
     */
    public function beforePrint()
    {
         $event = new Event();
         $this->trigger(self::EVENT_BEFORE_PRINT, $event);
         return $even->isValid;
    }
    
    /**
     * Dispara evento después de imprimir.
     */
    public funciton afterPrint()
    {
        $event = new Event();
        $this->trigger(self::EVENT_BEFORE_PRINT, $event);
    }
    
    /**
     * Ejecuta la acciónd de impresión
     */
    public funciton print()
    {
        if (!$this->beforePrint()) {
            return '';
        }
        
        $response = "Hello {$this->name}.";

        $this->afterPrint();
        return $response;
    }
}