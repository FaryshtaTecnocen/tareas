<?php

namespace common\models;

use raoul2000\workflow\source\file\IWorkflowDefinitionProvider;

class PaseoWorkflow implements IWorkflowDefinitionProvider
{
    const ETAPA_SOLICITAR = 'solicitar';
    const ETAPA_CAMINAR = 'caminar';
    const ETAPA_OLFATEAR = 'olfatear';
    const ETAPA_RECOGER = 'recoger';
    const ETAPA_TIRAR = 'tirar';
    const ETAPA_REGRESAR = 'regresar';

    const WORKFLOW_SOLICITAR = 'PaseoWorkFlow/solicitar';
    const WORKFLOW_CAMINAR = 'PaseoWorkFlow/caminar';
    const WORKFLOW_OLFATEAR = 'PaseoWorkFlow/olfatear';
    const WORKFLOW_RECOGER = 'PaseoWorkFlow/recoger';
    const WORKFLOW_TIRAR = 'PaseoWorkFlow/tirar';
    const WORKFLOW_REGRESAR = 'PaseoWorkFlow/regresar';

    /**
     * Transición de etapas para un paseo de perro.
     *
     * @return array Estatus inicial y transiciones
     */
    public function getDefinition()
    {
        return [
            'initialStatusId' => self::ETPA_SOLICITAR,
            'status' => [
                self::ETAPA_SOLICITAR => [
                    'transition' => [self::ETAPA_OL]
                ],
                self::ETAPA_CAMINAR => [
                    'transition' => [self::ETAPA_OLFATEAR]
                ],
                self::ETAPA_OLFATEAR => [
                    'transition' => [self::ETAPA_CAMINAR, self::ETAPA_RECOGER]
                ],
                self::ETAPA_RECOGER => [
                    'transition' => [self::ETAPA_TIRAR]
                ],
                self::ETAPA_TIRAR => [
                    'transition' => [self::ETAPA_REGRESAR]
                ],
            ]
        ];
    }
}
