<?php

namespace common\workflows\behaviors;

use common\workflows\PaseoWorkflow;
use common\models\Arbol;
use raoul2000\workflow\events\WorkflowEvent;
use yii\base\Behavior;
use yii\db\Expression as DbExpression;

class PaseoBehavior extends Behavior
{
    /**
     * @inheritdoc
     */
    public function events()
    {
        $enterRecoger = WorkflowEvent::afterEnterStatus(
            PaseoWorkflow::WORKFLOW_RECOGER
        );
        return [
            $enterRecoger => 'enterRecoger'
        ];
    }



    public function enterRecoger(WorkflowEvent $event)
    {
        $this->marcarArbol($event->sender->owner->arbol);
    }


    public function marcarArbol(Arbol $arbol)
    {
        $arbol->fecha_marcado = new DbExpression('date(now())');
        $arbol->save();
    }
}
